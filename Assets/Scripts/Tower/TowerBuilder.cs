﻿using System.Collections.Generic;
using UnityEngine;

public class TowerBuilder : MonoBehaviour
{
    [SerializeField] private float _towerSize;
    [SerializeField] private Transform _buildPoint;
    [SerializeField] private Block _blockPrefab;
    [SerializeField] private Color[] _colors;

    private List<Block> _blocks;

    public List<Block> Build()
    {
        _blocks = new List<Block>();
        Transform currentBuildPoint = _buildPoint;

        for (int i = 0; i < _towerSize; i++)
        {
            Block newBlock = BuildBlock(currentBuildPoint);
            newBlock.SetColor(_colors[Random.RandomRange(0, _colors.Length)]);
            _blocks.Add(newBlock);
            currentBuildPoint = newBlock.transform;
        }

        return _blocks;
    }

    private Block BuildBlock(Transform currentBuildPoint)
    {
        return Instantiate(_blockPrefab, GetBuildPoint(currentBuildPoint), Quaternion.identity, _buildPoint);
    }

    private Vector3 GetBuildPoint(Transform currentBlock)
    {
        return new Vector3(_buildPoint.position.x,
            currentBlock.position.y + _blockPrefab.transform.localScale.y / 10,
            _buildPoint.position.z);
    }
}