﻿using System;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;

[RequireComponent(typeof(TowerBuilder))]
public class Tower : MonoBehaviour
{
    private TowerBuilder _towerBuilder;
    private List<Block> _blocks;

    public event Action<int> SizeUpdated;


    private void Start()
    {
        _towerBuilder = GetComponent<TowerBuilder>();
        _blocks = _towerBuilder.Build();

        foreach (var block in _blocks)
        {
            block.BulletHited += OnBulletHited;
        }
        
        SizeUpdated?.Invoke(_blocks.Count);
    }

    private void OnBulletHited(Block hitedBlock)
    {
        hitedBlock.BulletHited -= OnBulletHited;

        _blocks.Remove(hitedBlock);

        foreach (var block in _blocks)
        {
            var position = block.transform.position;
            var localScale = block.transform.localScale;

            block.transform.position = new Vector3(position.x,
            position.y - localScale.y / 10,
            position.z);
        }
        
        SizeUpdated?.Invoke(_blocks.Count);
    }
}
