﻿using System;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class Block : MonoBehaviour
{
    [SerializeField] private ParticleSystem _destroyEffect;

    private MeshRenderer _meshRenderer;

    public event Action<Block> BulletHited;

    private void Awake()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
    }

    public void SetColor(Color color)
    {
        _meshRenderer.material.color = color;
    }

    public void Break()
    {
        BulletHited?.Invoke(this);

        var particleSystemRenderer = Instantiate(_destroyEffect,
            _destroyEffect.transform.position + new Vector3(0, 0.1f, 0),
            _destroyEffect.transform.rotation).GetComponent<ParticleSystemRenderer>();
        particleSystemRenderer.material.color = GetComponent<MeshRenderer>().material.color;

        Destroy(gameObject);
    }
}